<?php

namespace App\Http\Controllers\Api\V1;

use App\Companies;
use App\Http\Requests\Admin\UpdatecompanyRequest;
use Illuminate\Http\Request;
use App\Http\Resources\CompanyResource as companyresource;
use App\Http\Requests\Admin\StoreCompanyRequest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class CompanyController extends Controller
{
    public function index()
    {
        if (Gate::denies('user_access')) {
            return abort(401);
        }

        return new companyresource(Companies::with([])->get());
    }
//    show

    public function show($id)
    {
        if (Gate::denies('user_view')) {
            return abort(401);
        }

        $company = Companies::with([])->findOrFail($id);

        return new companyresource($company);
    }
//create
    public function store(StoreCompanyRequest $request)
    {
        if (Gate::denies('user_create')) {
            return abort(401);
        }

        $company = Companies::create($request->all());


        return (new companyresource($company))
            ->response()
            ->setStatusCode(201);
    }
//  update

    public function update(UpdatecompanyRequest $request, $id)
    {
        if (Gate::denies('user_edit')) {
            return abort(401);
        }

        $company = Companies::findOrFail($id);
        $company->update($request->all());



        return (new companyresource($company))
            ->response()
            ->setStatusCode(202);
    }
// delete

    public function destroy($id)
    {

        $company = Companies::findOrFail($id);
        $company->delete();

        return response(null, 204);
    }
}
