
//////////////////  start funtion vuex /// initialstate, getters ,actions, mutations ////////////////////////


function initialState() {
    return {
        item: {
            id: null,
            name: null,
            email: null,
            website: null,
            logo: null,
        },
        rolesAll: [],

        loading: false,
    }
}


const getters = {
    item: state => state.item,
    loading: state => state.loading,
    rolesAll: state => state.rolesAll,

}

const actions = {

    ////////////////// start create data ////////////////////////


    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.role)) {
                params.delete('role')
            } else {
                for (let index in state.item.role) {
                    params.set('role['+index+']', state.item.role[index].id)
                }
            }

            axios.post('/api/v1/company', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },

    ////////////////// end create data \\\\\\\\\\\\\\\


    ////////////////// start for update \\\\\\\\\\\\\\\

    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.role)) {
                params.delete('role')
            } else {
                for (let index in state.item.role) {
                    params.set('role['+index+']', state.item.role[index].id)
                }
            }

            axios.post('/api/v1/company/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },

    ////////////////// end update date ////////////////////////


    ////////////////// start  show data use fetch data ////////////////////////

    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/company/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchRolesAll')
    },
    fetchRolesAll({ commit }) {
        axios.get('/api/v1/roles')
            .then(response => {
                commit('setRolesAll', response.data.data)
            })
    },

    ////////////////// end show data use fetch data ////////////////////////



    ////////////////// start for set data into database in create.vue /// call funtion set ////////////////////////

    setName({ commit }, value) {
        commit('setName', value)
    },
    setEmail({ commit }, value) {
        commit('setEmail', value)
    },
    setWebsite({ commit }, value) {
        commit('setWebsite', value)
    },
    setLogo({ commit }, value) {
        commit('setLogo', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}
//////////////////start vuex ////////////////////////

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setName(state, value) {
        state.item.name = value
    },
    setEmail(state, value) {
        state.item.email = value
    },
    setWebsite(state, value) {
        state.item.website = value
    },
    setLogo(state, value) {
        state.item.logo = value
    },
    setRolesAll(state, value) {
        state.rolesAll = value
    },

    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

////////////////// end funtion vuex /// initialstate, getters ,actions, mutations ////////////////////////


///////////////// export funtion vuex ////////////////////

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
