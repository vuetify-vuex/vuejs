<?php

use Illuminate\Database\Seeder;

class CompanySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'phyrun', 'email' => 'phyrun@gmail.com', 'website'=>'www.phyrun@gmail.com.kh', 'logo' => 'http://www.vancouversun.com/cms/binary/10853221.jpg',],
            ['id' => 2, 'name' => 'luci', 'email' => 'luci@gmail.com', 'website'=>'www.luci@gmail.com.kh', 'logo' => 'http://www.vancouversun.com/cms/binary/10853221.jpg',],

        ];

        foreach ($items as $item) {
            \App\Companies::create($item);
        }
    }
}
